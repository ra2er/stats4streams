import os

from werkzeug.routing import Map, Rule
from werkzeug.serving import run_simple
from werkzeug.wsgi import DispatcherMiddleware

from auth.wsgi import auth_app
from utils.wsgi import WebApp


root_template_dir = os.path.join(os.path.dirname(__file__), 'templates')


class Stats4Streams(WebApp):

    rules = [Rule('/', endpoint='home-page')]

    def __init__(self, **kwargs):
        url_map = Map(self.rules)
        template_dirs = [root_template_dir]
        super(Stats4Streams, self).__init__(url_map=url_map,
                                            template_dirs=template_dirs)

    def home_page(self, request):
        context = self.get_context_data(request)
        return self.render_to_response('home_page.html', context=context)

stats4streams_app = Stats4Streams()
auth_app.template_provider.register_template_dir(root_template_dir)
app = DispatcherMiddleware(stats4streams_app, {
    '/auth': auth_app
})


if __name__ == '__main__':
    run_simple('127.0.0.1', 5000, app, use_debugger=True, use_reloader=True)

