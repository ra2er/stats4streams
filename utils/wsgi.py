from werkzeug.exceptions import HTTPException
from werkzeug.wrappers import Request, Response
import templating


class App(object):

    Request = Request
    Response = Response

    def __init__(self, url_map):
        self.url_map = url_map

    def dispatch_request(self, url_dispatcher, request):
        try:
            view, args = url_dispatcher.match(request.path)
        except HTTPException, e:
            response = e
        else:
            response = getattr(self, '%s' % view.replace('-', '_'))(request,
                                                                    **args)
        return response

    def wsgi_app(self, environ, start_response):
        try:
            url_dispatcher = self.url_map.bind_to_environ(environ)
        except Exception:
            raise
        else:
            response = self.dispatch_request(url_dispatcher,
                                             self.Request(environ))
        return response(environ, start_response)

    def __call__(self, environ, start_response):
        return self.wsgi_app(environ, start_response)


class WebApp(App):

    def __init__(self, template_dirs, **kwargs):
        self.template_provider = templating.Jinja2TemplateProvider(template_dirs)
        super(WebApp, self).__init__(**kwargs)

    def render_to_response(self, template, context, mimetype='text/html'):
        template = self.template_provider.render_to_string(template,
                                                           context=context)
        return self.Response(template, mimetype=mimetype)

    def get_context_data(self, request, **context):
        return dict(**context)
