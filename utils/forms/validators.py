import re
from flatland.validation import Validator

_ = lambda s: s


class MinLengthValueValidator(Validator):

    default_message = _(u'Ensure this value has at least '
                        u'%(limit_value)d characters (it has %(show_value)d).')

    def __init__(self, min_length, **kwargs):
        self.min_length = min_length
        super(MinLengthValueValidator, self).__init__(**kwargs)

    def validate(self, element, state):
        if len(element.value) < self.min_length:
            self.note_error(element, state,
                            message=self.default_message % {
                                'limit_value': self.min_length,
                                'show_value': len(element.value)
                            })
            return False
        return True


class MaxLengthValueValidator(Validator):

    default_message = _(u'Ensure this value has at most '
                        u'%(limit_value)d characters (it has %(show_value)d).')

    def __init__(self, max_length, **kwargs):
        self.max_length = max_length
        super(MaxLengthValueValidator, self).__init__(**kwargs)

    def validate(self, element, state):
        if len(element.value) > self.max_length:
            self.note_error(element, state,
                            message=self.default_message % {
                                'limit_value': self.max_length,
                                'show_value': len(element.value)
                            })
            return False
        return True


class RegexValidator(Validator):

    default_message = _('')

    def __init__(self, pattern, **kwargs):
        self.pattern = pattern
        super(RegexValidator, self).__init__(**kwargs)

    def validate(self, element, state):
        match = re.match(self.pattern, element.value)
        if match is None:
            self.note_error(element, state,
                            message=self.default_message)


