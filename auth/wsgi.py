import os
from werkzeug.routing import Rule, Map
from utils.wsgi import WebApp


class AuthApp(WebApp):

    rules = [Rule('/login/', endpoint='login')]

    def __init__(self, **kwargs):
        url_map = Map(self.rules)
        template_dirs = [os.path.join(os.path.dirname(__file__), 'templates')]
        super(AuthApp, self).__init__(url_map=url_map,
                                      template_dirs=template_dirs)

    def login(self, request):
        context = {}
        return self.render_to_response('login.html', context=context)


auth_app = AuthApp()