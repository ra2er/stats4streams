from sqlalchemy import Column, Integer, String, Boolean
from sqlalchemy.ext.declarative import declarative_base
from werkzeug.security import generate_password_hash, check_password_hash

Base = declarative_base()


class User(Base):

    UNUSABLE_PASSWORD = '!'

    __tablename__ = 'user'

    id = Column(Integer, primary_key=True)
    email = Column(String(80))
    password = Column(String(128))
    is_admin = Column(Boolean)

    def set_password(self, raw_password):
        if raw_password is None:
            self.password = User.UNUSABLE_PASSWORD
        else:
            self.password = generate_password_hash(raw_password)

    def check_password(self, raw_password):
        return (self.has_usable_password() and
                check_password_hash(self.password, raw_password))

    def has_usable_password(self):
        return (self.password is not None
                and self.password != User.UNUSABLE_PASSWORD
                and '$' in self.password)