from sqlalchemy import Column, Integer, String, ForeignKey
from sqlalchemy.ext.declarative import declarative_base

from ..auth.models import User


Base = declarative_base()


class Station(Base):

    __tablename__ = 'station'

    id = Column(Integer(), primary_key=True)
    name = Column(String(100))
    owner_id = Column(Integer, ForeignKey(User.id), nullable=False)
